﻿using UnityEngine;
using System.Collections;

public class Object : MonoBehaviour {
	[SerializeField] private float resetPos = -28.6f;
	[SerializeField] protected float startPos = 66.6f;
	private Vector3 replayPosition;
	[SerializeField] private float objectSpeed = 1;
	// Use this for initialization
	void Start () {
		replayPosition = transform.position;
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		if(!GameManager.instance.GameOver()) {
			transform.Translate(Vector3.left * objectSpeed * Time.deltaTime, Space.World);

			if(transform.localPosition.x <= resetPos) {
				Vector3 newPos = new Vector3(startPos, transform.position.y, transform.position.z);
				transform.position = newPos;
			}
		} else if (GameManager.instance.GameOver()) {
			transform.position = replayPosition;
		}

	}
}
