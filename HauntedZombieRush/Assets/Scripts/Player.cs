﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;

public class Player : MonoBehaviour {

	private Animator anim;
	private Rigidbody rigidBody;
	private AudioSource audioSource;
	private bool jump = false;

	[SerializeField] private float jumpForce = 100.0f;
	[SerializeField] private AudioClip sfxJump;
	[SerializeField] private AudioClip sfxDeath;
	private Vector3 startingPos;
	[SerializeField] private Vector3 startRot;

	void Awake () {
		Assert.IsNotNull(sfxJump);
		Assert.IsNotNull(sfxDeath);
	}

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		rigidBody = GetComponent<Rigidbody>();
		audioSource = GetComponent<AudioSource>();
		startingPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
		if(!GameManager.instance.GameOver() && GameManager.instance.GameStarted()) {
			if(Input.GetMouseButtonDown(0)) {
				GameManager.instance.PlayerStartedGame();
				anim.Play("Jump");
				audioSource.PlayOneShot(sfxJump);
				rigidBody.useGravity = true;

				jump = true;
			}
		}
		if(GameManager.instance.GameOver()) {
			rigidBody.velocity = new Vector2(0, 0);
			transform.position = startingPos;
			rigidBody.useGravity = false;
		}


	}

	void FixedUpdate() {
		if(jump) {
			jump = false;
			rigidBody.velocity = new Vector2(0, 0);
			rigidBody.AddForce(new Vector2(0, jumpForce), ForceMode.Impulse);
		}
	}

	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.tag == "obstacle") {
			rigidBody.AddForce(new Vector2(-50, 20), ForceMode.Impulse);
			rigidBody.detectCollisions = false;
			audioSource.PlayOneShot(sfxDeath);
			GameManager.instance.PlayerCollided();
		}
	}
}
